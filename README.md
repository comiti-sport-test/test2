Test technique pour Comiti-Sport visant à développer une couche d’accès aux données qui écrit dans un fichier au
format json.


DAOFactory:
    Sert à créer des instance de DAO pour chaque entité

IDAO:
    Interface de DAO

ADAO:
    DAO primaire pour chaque DAO capable de Créer, Modifier, Rechercher et Supprimer des élements du Json correspondant à l'entité
    
CharacterDao:
    Dao personnalisé pour gérer l'entité Character

ArmyDao:
      Dao personnalisé pour gérer l'entité Army


Design patern utilisé: Singleton, Factory

-Pas de gestion d'unicité des élements
-Pas de gestion des relatiosn entre entités
-Pas d'inversion de contrôle
