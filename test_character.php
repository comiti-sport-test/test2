<?php

require_once 'DAO/DaoFactory.php';
require_once 'DAO/Request.php';

$daoFactory = DAOFactory::GetInstance();
$daoCharacter = $daoFactory->GetCharacterDao();
$c = new Character("1");
$c2 = new Character("2");
$c3 = new Character("3");
// $c4 = new Character("4");

$daoCharacter->Create($c);
$daoCharacter->Create($c2);
$daoCharacter->Create($c3);

$dRequest = new Request($_select="_name", $_from="Character",  $_where="1");
$fRequest = new Request($_select="_name", $_from="Character",  $_where="2");
$uRequest = new Request($_select="_name", $_from="Character",  $_where="3");

$daoCharacter->Delete($dRequest);
$res = $daoCharacter->Find($fRequest);
$daoCharacter->Update($uRequest, "42");
print_r($res);
