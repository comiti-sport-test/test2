<?php

/**
 * interface des DAO
 * @var IDAO
 */
interface IDAO {

    /**
     * Action commune a tous les DAO qui permet d'enregistrer une class dans la BDD Json
     * en la convertissant en Entity
     * @param  [type] $class class à enregistrer
     * @return int           retourn 84 en cas d'échec, 0 sinon
     */
    public function Create($class) : int;

    /**
    *Action commune a tous les DAO qui permet de récupéerer les entitées présentent
    * dans un tableau selon la $request
    * @param  Request $request qui permet de sélectionnées les entitées
    * @return array    array contenant les entitées trouvées
     */
    public function Find(Request $request) : array;

    /**
     * Action commune a tous les DAO qui permet de modifier une entité
     * @param  Request $request qui permet de sélectionnées les entitées
     * @param  [type]  $updated valeur à inscrire
     *  @return int    retourn 84 en cas d'échec, 0 sinon
     */
    public function Update(Request $request, $updated) : int;
    public function Delete(Request $request) : int;
}

/**
 * Class permettant de gérer les tableau dans la BDD Json
 */
class Entity
{
    public $_id = 0;
    public $_instance;

    public function __construct($instance, $id)
    {
        $this->_instance = $instance;
        $this->_id = $id;
    }
}
