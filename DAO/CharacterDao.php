<?php

require_once 'DAO/ADao.php';


/**
 * Class test
 */
class Character {
    public $_name;
    /**
     * constructeur
     */
     public function __construct(string $name)
     {
         $this->_name = $name;
     }
}


/**
 * DAO pour la class Character
 */
class CharacterDao extends ADao
{

    private static $instance = null;

    public static function GetInstance(DaoFactory $instance) : CharacterDao
   {
        if (self::$instance == null)
           self::$instance = new CharacterDao($instance);
         return self::$instance;
     }


}
