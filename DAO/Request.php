<?php

class Request {
    /**
     * Equivalant au champ SELECT en sql
     * @var [type]
     */
    public $_select;

    /**
     * Equivalant au champ FROM en sql
     * @var [type]
     */
    public $_from;

    /**
     * Equivalant au champ WHERE en sql
     * @var [type]
     */
    public $_where;

    /**
     * constructor
     * @param string $select Nom de la proprieté (= champ en sql)
     * @param string $from   Nom du Json (= tabkeau en sql)
     * @param  $where  valeur attendu
     */
     public function __construct(string $select = "", string $from  = "", $where  = 0)
     {
         $this->_select = $select;
         $this->_from = $from;
         $this->_where = $where;
     }
}
