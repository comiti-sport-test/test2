<?php

require_once 'DAO/ADao.php';


/**
 * Class test
 */
class Army {
    public $_name;
    public $_weapons = 120;

    /**
     * constructeur
     */
     public function __construct(string $name)
     {
         $this->_name = $name;
     }

}


/**
 * DAO pour la class Army
 */
class ArmyDao extends ADao
{

    private static $instance = null;

    public static function GetInstance(DaoFactory $instance) : ArmyDao
   {
        if (self::$instance == null)
           self::$instance = new ArmyDao($instance);
         return self::$instance;
     }



}
