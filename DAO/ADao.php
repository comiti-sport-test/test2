<?php

require 'DAO/IDao.php';


/**
 *  DAO primaire dont tous les DAO hérite
 */
class ADao implements IDAO
{
    /**
     * instance du DAOFactory pour utiliser des utilitaires commun
     * @var [type]
     */
    private $_daoFactory;

    /**
     * constructor
     * @param DaoFactory $daoFactory instance du DAOFactory
     */
    public function __construct($daoFactory)
    {
        $this->_daoFactory = $daoFactory;
    }


    /**
     *Creer une Entity et l'ajoute au tableau correspondant dans la BDD Json
     * @param  $elem type de donné transformé en Entity
     * @return int  return toujours 0 /!\ pas de gestion d'erreur
     */
    public function Create($elem) : int {
        if ( gettype($elem) == "null" || gettype($elem) == "unknow type")
            return 84;
        if (gettype($elem) == "object")
            $className = get_class($elem);
        else
            $className = gettype($elem);
        $entity = $this->_daoFactory->CreateEntity($elem, $className);
        $jsonFile = $className . ".json";
        if (file_exists($jsonFile)) {
            $json_arr = $this->_daoFactory->GetJson($className);
            if ($json_arr != null) {
                $json_arr[] = $entity;
            } else
                $json_arr[] = $entity;
        } else
            $json_arr[] = $entity;
        file_put_contents($jsonFile, json_encode($json_arr));
        return 0;
    }

    /**
     * Retourne une array d'élément suivant la $request envoyé
     * @param  Request $request qui permet de cibler les éléments
     * @return array   Array contenant les éléments
     */
    public function Find(Request $request) : array
    {

        $entities = $this->_daoFactory->RequestEntity($request);
        $result = [];
        foreach ($entities as $id => $Aobj) {
           $obj = $Aobj["_instance"];
            if (array_key_exists($request->_select, $obj)  && $obj[$request->_select] == $request->_where || $request->_where == "")
                array_push($result, $Aobj);
        }
        return $result;
    }

    /**
     * Supprime les éléments correpsondant à la $request
     * @param  Request $request qui permet de cibler les éléments
     * @return int  return toujours 0 /!\ pas de gestion d'erreur

     */
    public function Delete(Request $request) : int
    {
        $json_arr = $this->_daoFactory->GetJson($request->_from);
        $entities = $this->_daoFactory->RequestEntity($request);
        $arr_index = array();
        if(count($entities) == 0)
            return 84;
        foreach ($json_arr as $id => $entity)
        {
            $obj = $entity["_instance"];
            if (array_key_exists("_id", $entity) && $entity["_id"] == $entities[0]["_id"])
            {
                $arr_index[] = $id;
            }
        }

        foreach ($arr_index as $i)
        {
            unset($json_arr[$i]);
        }
        $json_arr = array_values($json_arr);
        file_put_contents($request->_from . ".json", json_encode($json_arr));
        return 0;

    }


    /**
    * Modifier les éléments correpsondant à la $request par la valeur $update
     * @param  Request $request qui permet de cibler les éléments
     * @param  [type]  $updated nouvelle donné à inscrire
     * @return int    Retourne 84 en cas d'échec, sinon 0
     */
    public function Update(Request $request, $updated) : int
    {
        $json_arr = $this->_daoFactory->GetJson($request->_from);
        $entities = $this->_daoFactory->RequestEntity($request);

        if(count($entities) == 0)
            return 84;

        // print_r($entities);

        foreach ($json_arr as $id => $entity) {
            if (array_key_exists("_id", $entity) && $entity["_id"] == $entities[0]["_id"]) {
                $json_arr[$id]["_instance"][$request->_select] = $updated;
                // printf("id: %s\n", $id);
            }
        }
        file_put_contents($request->_from . ".json", json_encode($json_arr));
        return 0;
    }

}
