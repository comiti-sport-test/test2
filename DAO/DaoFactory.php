<?php


require_once 'DAO/CharacterDao.php';
require_once 'DAO/ArmyDao.php';
require_once 'DAO/Request.php';


class DAOFactory {
    /**
     * instance unique du DAOFactory
     * @var DAOFactory
     */
     private static $instance = null;


     /**
      * constructeur
      */
      private function __construct() {}


     /**
      * utilitaires afin de récupérer le tableau dans la BDD Json
      * @param string $className Nom de l'entité
      */
     public function GetJson(string $className)
     {
         if (!file_exists($className . ".json"))
            return [];
         $jsonFile = $className . ".json";
         $data = file_get_contents($jsonFile);
         $json_arr = json_decode($data, true);
         return $json_arr;
     }


     /**
      * utilitaires qui retourne les entité dans le tableau qui correspond à la $request sous forme d'array
      * @param  Request $request  qui permet de cibler les éléments
      * @return array   contien les entités sélectionnées
      */
     public function RequestEntity(Request $request) : array {
         $resultat = [];

         $json_arr = $this->GetJson($request->_from);
         if ($json_arr == null)
             return [];
         $result = [];
         foreach ($json_arr as $id => $Aobj) {
            $obj = $Aobj["_instance"];
            // printf("test: %s : %s\n", $request->_where, $obj[$request->_select]);

             if (array_key_exists($request->_select, $obj)  && $obj[$request->_select] == $request->_where) {
                 // printf("test: %s : %s\n", $request->_where, $obj[$request->_select]);
                 array_push($result, $Aobj);

             }
         }
         return $result;
     }

     /**
      * utilitaires qui génère un id non utilisé dan un tableau $className donné
      * @param  string $className nom du tableau dans la BDD Json
      * @return int     retourn 0 s'il n'y a pas d'élement, sinon retourne un id non utilisé
      */
     public function GenerateId(string $className) : int
     {
         $result = 0;
         $json_arr = $this->GetJson($className);
         if ($json_arr == null)
             return 0;
         $check = 1;
         while ($check) {
             $check = 0;
             foreach ($json_arr as $keu => $entity) {
                 if (array_key_exists("_id", $entity) && $entity["_id"] == $result)
                     $check = 1;
             }
             if ($check)
                $result += 1;
        }
        return $result;
     }

     /**
      * Créer une entité qui permet de gérer les tableau dans la BDD Json grâce aux id
      * @param  [type] $elem        instance de k'ékément
      * @param  [type] $className   nom du futur tableau dans lequel l'entité correspondra
      * @return Entity            Entité générée
      */
     public function CreateEntity($elem, $className) : Entity {
         $id = $this->GenerateId($className);
         return new Entity($elem, $id);
     }


    /**
     * retourne l'instance du DAOFactory
     * @return DAOFactory
     */
     public static function GetInstance() : DAOFactory
    {
      if (self::$instance == null) {
        self::$instance = new DAOFactory();
      }
      return self::$instance;
    }



    /**
     * Créer un CharacterDao
     * @return CharacterDao DAO généré par la factory
     */
    public function GetCharacterDao() : CharacterDao
    {
        return CharacterDao::GetInstance(($this->GetInstance()));
    }

    /**
     * Créer un ArmyDao
     * @return ArmyDao DAO généré par la factory
     */
    public function GetArmyDao() : ArmyDao
    {
        return ArmyDao::GetInstance(($this->GetInstance()));
    }
}
